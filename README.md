---
__Mini Project__

- __Purpose__ - This system is used to manage projects.
- __Number of screens__ : 1.
- __Function__ :
  1. Project CRUD
  2. Edit user profile
  3. Change Password


Project will be expanded to serve the project management of a company/team in the software field.
Later, there will be screens to permit project members, manage project progress, overview the project process according to spring, manage tasks, ...!

---

## Project Manager 
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ProjectManagerScreen1.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ProjectManagerScreen1.png

Đây là màn hình chính. Tại đây sẽ hiển thị danh sách các project mà user đang tham gia. 

Khi click vào icon tab, sẽ hiển thị phần leftbar như sau: 
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ProjectManagerScreen2.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ProjectManagerScreen2.png


---

## Create/edit project
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/Create_editproject.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/Create_editproject.png

- Đây là màn hình create/edit project. 
   1. Khi click vào button 'Create New Project ' sẽ hiển thị ra popup với tiêu đề 'Add new project'. Các field trong popup sẽ hiển thị trống và user sẽ nhập những nội dung này . 
       - Sau khi nhập đầy đủ nội dung, click button 'Save' để tiến hành lưu project. 
   2. Khi click vào icon 'Edit' ở mỗi project, sẽ hiển thị popup. Nội dung trong popup sẽ tương ứng ới nội dung project đang được edit. 
   
---

## Delete project
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/DeleteProject.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/DeleteProject.png

- Đây là màn hình delete project. 
   - Khi click vào icon 'delete' ở mỗi project, sẽ hiển thị popup xác nhận xóa project. Click button 'Yes, delete it!' để xác nhận xóa project. 
   
---

## Edit profile 
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/EditProfile.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/EditProfile.png

Đây là màn hình edit profile của user. 
   - Khi click vào icon 'user' ở leftbar hoặc avatar user, popup sẽ hiển thị. Nội dung trong popup sẽ là thông tin user hiện tại. 
   - Đối với Avatar : Khi click vào avatar , sẽ tiến hành chọn ảnh từ thư viện. Sau khi chọn hoàn thành, avatar mới sẽ được lưu mà không cần phải qua xác nhận nào khác . 
   - Sau khi hoàn thành nhập các thông tin thay đổi, click button "Save" để lưu thông tin . 
   

---

## Change Password
![UI](https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ChangePassword.png)
- Link: https://gitlab.com/NganTran1209/miniproject_techmaster/-/blob/main/PNG/ChangePassword.png

Đây là popup Change Password. Popup này hiển thị trên cùng 1 popup với Edit profile. 
   - Khi click vào button  'Change Password' ở popup Edit Profile , nội dung popup sẽ thay đổi mà không gây mất/tắt popup 'Edit profile'. 
   - Click button 'Edit Profile ' để quay về nội dung ở màn hình Edit profile. 
   





